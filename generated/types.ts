export type GetFooHeaders = unknown;
export interface GetFooQuery {
  foo?: number;
  bar?: number;
}
export type GetFooParams = unknown;
export type PostFooHeaders = unknown;
export type PostFooQuery = unknown;
export type PostFooParams = unknown;
export interface PostFooBody {
  foo: string;
  bar?: string | null;
  baz?: number | null;
}
export interface GetFooIdHeaders {
  "x-foo"?: string;
  [k: string]: any;
}
export interface GetFooIdQuery {
  foo?: number;
  bar?: number;
}
export interface GetFooIdParams {
  id: number;
}
