// Ignore this file. Was working on this until
// I saw https://github.com/wesleytodd/express-openapi

const fs = require("fs");
const path = require("path");

function genOpenAPIDoc(app) {
  const routes = getRoutes(app);
  console.log("routes", routes);

  for (let route of routes) {
    const pth = route.path;
    const handler = last(route.stack);
    if (handler.schema) {
    }
  }
}

module.exports.genOpenAPIDoc = genOpenAPIDoc;

function getRoutes(app) {
  let routes = [];

  for (let middleware of app._router.stack) {
    if (middleware.route) {
      // routes registered directly on the app
      routes.push(middleware.route);
    } else if (middleware.name === "router") {
      // router middleware
      for (let handler of middleware.handle.stack) {
        if (handler.route) {
          routes.push(handler.route);
        }
      }
    }
  }

  return routes;
}

function last(xs) {
  return xs[xs.length - 1];
}
