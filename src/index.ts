import * as fs from "fs";
import * as path from "path";
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
import configureApp from "./routes";
const config = require("./config");
const { getSource } = require("./gen-types");

const app = express();
app.set("x-powered-by", false);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require("./oapi").default);
app.use("/docs", require("./oapi").default.swaggerui);

// app.use(routes);
configureApp(app);

app.listen(config.port, err => {
  if (err) {
    throw err;
  }
  console.log(`server listening on ${config.port}`);

  if (process.env.NODE_ENV !== "production") {
    const pth = path.join(__dirname, "../generated/types.ts");
    getSource(app._router).then(src => {
      if (src !== fs.readFileSync(pth, { encoding: "utf8" })) {
        fs.writeFileSync(pth, src);
      }
    });
  }
});
