// import { Request } from "express";
import oapi from "./oapi";
import * as t from "../generated/types";

// const router = require("express").Router();

export default function(router) {
  router.get(
    "/foo",
    oapi.validPath({
      parameters: [
        { name: "foo", in: "query", schema: { type: "number" } },
        { name: "bar", in: "query", schema: { type: "number" } }
      ],
      responses: {
        200: {
          content: {
            "application/json": {
              schema: {
                type: "object",
                properties: {
                  hello: { type: "string" }
                },
                additionalProperties: false,
                required: ["hello"]
              }
            }
          }
        }
      }
    }),
    (
      req: {
        params: t.GetFooParams;
        query: t.GetFooQuery;
        headers: t.GetFooHeaders;
      },
      res
    ) => {
      return res.json({ hello: "world" });
    }
  );

  router.get(
    "/foo/:id",
    oapi.validPath({
      parameters: [
        { name: "foo", in: "query", schema: { type: "number" } },
        { name: "bar", in: "query", schema: { type: "number" } },
        { name: "id", in: "path", required: true, schema: { type: "number" } },
        { name: "x-foo", in: "header", schema: { type: "string" } }
      ],
      responses: {
        200: {
          content: {
            "application/json": {
              schema: {
                type: "object",
                properties: {
                  hello: { type: "string" }
                },
                additionalProperties: false,
                required: ["hello"]
              }
            }
          }
        }
      }
    }),
    (
      req: {
        params: t.GetFooIdParams;
        query: t.GetFooIdQuery;
        headers: t.GetFooIdHeaders;
      },
      res
    ) => {
      return res.json({ hello: `hi, ${req.params.id}!` });
    }
  );

  router.post(
    "/foo",
    oapi.validPath({
      requestBody: {
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                foo: { type: "string" },
                bar: { type: "string", nullable: true },
                baz: { type: "number", nullable: true }
              },
              additionalProperties: false,
              required: ["foo"]
            }
          }
        }
      },
      responses: {
        200: {
          content: {
            "application/json": {
              schema: {
                type: "object",
                properties: {
                  bar: { type: "string" }
                },
                additionalProperties: false,
                required: ["bar"]
              }
            }
          }
        }
      }
    }),
    (
      req: {
        body: t.PostFooBody;
        params: t.PostFooParams;
        query: t.PostFooQuery;
        headers: t.PostFooHeaders;
      },
      res
    ) => {
      return res.json({ bar: "hi " + req.body.foo });
    }
  );
}
