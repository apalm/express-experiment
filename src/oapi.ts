const openapi = require("@wesleytodd/openapi");

export default openapi({
  info: {
    title: "Test",
    description: "Test",
    version: "1.0.0"
  },
  externalDocs: {
    url: "https://swagger.io",
    description: "Find more info here"
  },
  consumes: ["application/json"],
  produces: ["application/json"]
});
